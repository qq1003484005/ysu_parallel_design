
//  * Run:      mpiexec -n <number of processes> ./mpi_nbody_basic

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tree.h"
#include "body.h"
#include "mpi.h"

#define FILENAME "input_file_8.txt"
#define MASTER 0

//#define GENERATE_INPUT_FILE 1
#define DEBUG_GET_ARGUMENT 1
//#define READ_GENERATE_BODIES 1
//#define DEBUG_READ_FILE 1
#define DEBUG_OUTPUT_STATE 1
#define GENERATE_OUTPUT_FILE 1

//const double G = 6.673e-11;
const double G = 6;

const double treeratio = 1;

int my_rank; //进程编号
int size;    //进程数量

void Help();
void Get_Input_Arguments(int argc, char *argv[], int *num_particles, int *num_steps, double *delta_t, int *output_freq, char *init_condition);
struct body *Read_File_Init_Conditons(char *filename, struct body *bodies, int num_particles);
struct body *Generate_Init_Conditions(const int num_particles);
void Output_State(double time, struct body *bodies, int num_particles);
void Generate_Output_File(struct body *bodies, int num_particles);

double min(double a, double b);
double max(double a, double b);

enum quadrant
{
    NO,
    NW,
    SW,
    SO
};

enum quadrant Get_Quadrant(double x, double y, double xmin, double xmax, double ymin, double ymax);
void Update_Center_Mass(struct node *nodep, struct body *bodyp);

enum quadrant Get_Quadrant(double x, double y, double xmin, double xmax, double ymin, double ymax)
{

    double midx, midy;

    midx = xmin + 0.5 * (xmax - xmin);
    midy = ymin + 0.5 * (ymax - ymin);

    if (y > midy)
    {
        if (x > midx)
            return NO;
        else
            return NW;
    }
    else
    {
        if (x > midx)
            return SO;
        else
            return SW;
    }
}

struct node *Create_Node(struct body *bodyp, double xmin, double xmax, double ymin, double ymax)
{
    struct node *rootnode;
    if (!(rootnode = malloc(sizeof(struct node))))
    {
        printf("Impossibile allocare il nodo\n");
    }

    rootnode->totalmass = bodyp->m;
    rootnode->centerx = bodyp->x;
    rootnode->centery = bodyp->y;
    rootnode->xmin = xmin;
    rootnode->xmax = xmax;
    rootnode->ymin = ymin;
    rootnode->ymax = ymax;

    rootnode->diag = sqrt((pow(xmax - xmin, 2) + pow(ymax - ymin, 2)));

    rootnode->bodyp = bodyp;

    rootnode->NO = NULL;
    rootnode->NW = NULL;
    rootnode->SW = NULL;
    rootnode->SO = NULL;

    return rootnode;
}

void Insert_Body(struct body *insbody, struct node *nodep)
{

    enum quadrant existingquad, newquad;
    double xmid, ymid;

    xmid = nodep->xmin + 0.5 * (nodep->xmax - nodep->xmin);
    ymid = nodep->ymin + 0.5 * (nodep->ymax - nodep->ymin);

    if (nodep->bodyp != NULL)
    {
        existingquad = Get_Quadrant(nodep->bodyp->x, nodep->bodyp->y, nodep->xmin, nodep->xmax, nodep->ymin, nodep->ymax);

        switch (existingquad)
        {
        case NO:
            nodep->NO = Create_Node(nodep->bodyp, xmid, nodep->xmax, ymid, nodep->ymax);
            break;
        case NW:
            nodep->NW = Create_Node(nodep->bodyp, nodep->xmin, xmid, ymid, nodep->ymax);
            break;
        case SW:
            nodep->SW = Create_Node(nodep->bodyp, nodep->xmin, xmid, nodep->ymin, ymid);
            break;
        case SO:
            nodep->SO = Create_Node(nodep->bodyp, xmid, nodep->xmax, nodep->ymin, ymid);
            break;
        }

        nodep->bodyp = NULL;
    }

    newquad = Get_Quadrant(insbody->x, insbody->y, nodep->xmin, nodep->xmax, nodep->ymin, nodep->ymax);
    Update_Center_Mass(nodep, insbody);

    switch (newquad)
    {
    case NO:
        if (nodep->NO == NULL)
        {
            nodep->NO = Create_Node(insbody, xmid, nodep->xmax, ymid, nodep->ymax);
        }
        else
        {
            Insert_Body(insbody, nodep->NO);
        }
        break;
    case NW:
        if (nodep->NW == NULL)
        {
            nodep->NW = Create_Node(insbody, nodep->xmin, xmid, ymid, nodep->ymax);
        }
        else
        {
            Insert_Body(insbody, nodep->NW);
        }
        break;
    case SW:
        if (nodep->SW == NULL)
        {
            nodep->SW = Create_Node(insbody, nodep->xmin, xmid, nodep->ymin, ymid);
        }
        else
        {
            Insert_Body(insbody, nodep->SW);
        }
        break;
    case SO:
        if (nodep->SO == NULL)
        {
            nodep->SO = Create_Node(insbody, xmid, nodep->xmax, nodep->ymin, ymid);
        }
        else
        {
            Insert_Body(insbody, nodep->SO);
        }
        break;
    }
}

void Update_Center_Mass(struct node *nodep, struct body *bodyp)
{
    nodep->centerx = (nodep->totalmass * nodep->centerx + bodyp->m * bodyp->x) / (nodep->totalmass + bodyp->m);
    nodep->centery = (nodep->totalmass * nodep->centery + bodyp->m * bodyp->y) / (nodep->totalmass + bodyp->m);
    nodep->totalmass += bodyp->m;
    return;
}

void Destroy_Tree(struct node *nodep)
{
    if (nodep != NULL)
    {
        if (nodep->NO != NULL)
            Destroy_Tree(nodep->NO);
        if (nodep->NW != NULL)
            Destroy_Tree(nodep->NW);
        if (nodep->SW != NULL)
            Destroy_Tree(nodep->SW);
        if (nodep->SO != NULL)
            Destroy_Tree(nodep->SO);

        free(nodep);
    }
}

void Tree_Sum(struct node *nodep, struct body *bodyp, double G, double treeratio)
{
    double dx, dy, len, len_3;
    double m_g;
    double fact;

    dx = nodep->centerx - bodyp->x;
    dy = nodep->centery - bodyp->y;

    len = sqrt(pow(dx, 2) + pow(dy, 2));
    len_3 = pow(len, 3);

    if ((((len / nodep->diag) > treeratio) || (nodep->bodyp)) && (nodep->bodyp != bodyp))
    {
        m_g = G * nodep->totalmass * bodyp->m;
        fact = m_g / len_3;

        bodyp->fx += fact * dx;
        bodyp->fy += fact * dy;
    }
    else
    {
        if (nodep->NO)
        {
            Tree_Sum(nodep->NO, bodyp, G, treeratio);
        }
        if (nodep->NW)
        {
            Tree_Sum(nodep->NW, bodyp, G, treeratio);
        }
        if (nodep->SW)
        {
            Tree_Sum(nodep->SW, bodyp, G, treeratio);
        }
        if (nodep->SO)
        {
            Tree_Sum(nodep->SO, bodyp, G, treeratio);
        }
    }
}

int main(int argc, char *argv[])
{

    int num_particles;   /* Total number of particles   总粒子数 */
    int steps;           /* Current step                当前步骤*/
    int num_steps;       /* Number of timesteps         数量的步伐*/
    double delta_t;      /* Size of timestep            步伐的大小*/
    int output_freq;     /* Frequency of output         输出频率*/
    char init_condition; /*_G_en or _i_nput init conds */
    struct body *bodies;
    double time; /* Current Time                当前时间*/
    double start_time;
    double end_time;

    //MPI初始化
    MPI_Init(&argc, &argv);
    //获得通信空间comm中规定的组包含的进程的数量
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    //获得当前进程在指定通信域中的编号
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    Get_Input_Arguments(argc, argv, &num_particles, &num_steps, &delta_t, &output_freq, &init_condition);

    if (my_rank == MASTER)
    {
        if (init_condition == 'g')
            bodies = Generate_Init_Conditions(num_particles);
        else
            bodies = Read_File_Init_Conditons(FILENAME, bodies, num_particles);
    }

    MPI_Bcast(&num_particles, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (my_rank != MASTER)
    {
        if (!(bodies = malloc(num_particles * sizeof(struct body))))
        {
            printf("Impossibile allocare memoria per %d bodies", num_particles);
            exit(1);
        }
    }

    int i;
    for (i = 0; i < num_particles; i++)
    {
        MPI_Bcast(&((bodies)[i].m), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&((bodies)[i].x), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&((bodies)[i].y), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&((bodies)[i].fx), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&((bodies)[i].fy), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    start_time = MPI_Wtime();

#ifdef DEBUG_OUTPUT_STATE
    if (my_rank == MASTER)
        Output_State(time, bodies, num_particles);
#endif

    int step;
    for (step = 1; step <= num_steps; step++)
    {
        time = step * delta_t;

        struct node *rootnode;
        double xmin = 0.0, xmax = 0.0;
        double ymin = 0.0, ymax = 0.0;

        for (i = 0; i < num_particles; i++)
        {
            bodies[i].fx = 0.0;
            bodies[i].fy = 0.0;
            xmin = min(xmin, bodies[i].x);
            xmax = max(xmax, bodies[i].x);
            ymin = min(ymin, bodies[i].y);
            ymax = max(ymax, bodies[i].y);
        }

        rootnode = Create_Node(bodies + 0, xmin, xmax, ymin, ymax);

        for (i = 1; i < num_particles; i++)
            Insert_Body(bodies + i, rootnode);

        for (i = my_rank; i < num_particles; i += size)
        {
            Tree_Sum(rootnode, bodies + i, G, treeratio);
        }

        for (i = 0; i < num_particles; i++)
        {
            MPI_Bcast(&(bodies[i].fx), 1, MPI_DOUBLE, i % size, MPI_COMM_WORLD);
            MPI_Bcast(&(bodies[i].fy), 1, MPI_DOUBLE, i % size, MPI_COMM_WORLD);
        }

        for (i = 0; i < num_particles; i++)
        {
            bodies[i].x += delta_t * bodies[i].vx;
            bodies[i].y += delta_t * bodies[i].vy;

            bodies[i].vx += bodies[i].fx * (delta_t / bodies[i].m);
            bodies[i].vy += bodies[i].fy * (delta_t / bodies[i].m);
        }

        Destroy_Tree(rootnode);

#ifdef DEBUG_OUTPUT_STATE
        if (steps % output_freq == 0)
            Output_State(time, bodies, num_particles);
#endif
    }

#ifdef GENERATE_OUTPUT_FILE
    if (my_rank == MASTER)
        Generate_Output_File(bodies, num_particles);
#endif

    end_time = MPI_Wtime();
    if (my_rank == MASTER)
    {
        printf("Tempo trascorso = %e seconds\n", end_time - start_time);
        fprintf(stderr, "%d,%d,%e\n", num_particles, size, end_time - start_time);
    }

    free(bodies);
    MPI_Finalize();
    return 0;
}

/*---------------------------------------------------------------------
 * Function: Usage
 * Purpose:  Print instructions for command-line and exit  打印命令行指令并退出
 * In arg:
 *    prog_name:  the name of the program as typed on the command-line  命令行上键入的程序名称
 */
void Help()
{
    fprintf(stderr, "   mpirun -np <numero processi> <nome programma>");
    fprintf(stderr, "   <numero particelle> <numbero di timesteps>");
    fprintf(stderr, "   <taglia timesteps> <output frequency>");
    fprintf(stderr, "   <f|g>");
    fprintf(stderr, "   'g': il programma genera le condizoni iniziali\n");
    fprintf(stderr, "   'f': leggo da file le condizioni iniziali\n");

    exit(0);
}

/*---------------------------------------------------------------------
 * Function:  Get_Input_Arguments
 * Purpose:   Prende i gli argomenti da linea di comando
 * In args:
 *    argc:            numero di argomenti
 *    argv:            array di argomenti
 * Out args:
 *    num_particles:    numero totale di particelle
 *    n_steps:          numero timesteps
 *    delta_t:          taglia timesteps
 *    output_freq:      frequenza output
 *    init_condition:   da dove leggere input
 */
void Get_Input_Arguments(int argc, char *argv[], int *num_particles, int *num_steps, double *delta_t, int *output_freq, char *init_condition)
{

    if (argc != 6)
        Help();

    *num_particles = strtol(argv[1], NULL, 10);
    *num_steps = strtol(argv[2], NULL, 10);
    *delta_t = strtod(argv[3], NULL);
    *output_freq = strtol(argv[4], NULL, 10);
    *init_condition = argv[5][0];

    if (num_particles <= 0 || *num_steps < 0 || *delta_t <= 0)
    {
        if (my_rank == MASTER)
            Help();
        MPI_Finalize();
        exit(0);
    }
    if (*init_condition != 'g' && *init_condition != 'f')
    {
        if (my_rank == MASTER)
            Help();
        MPI_Finalize();
        exit(0);
    }

#ifdef DEBUG_GET_ARGUMENT
    if (my_rank == MASTER)
    {
        printf("num_particles = %d\n", *num_particles);
        printf("num_steps = %d\n", *num_steps);
        printf("delta_t = %e\n", *delta_t);
        printf("output_freq = %d\n", *output_freq);
        printf("init_conditon = %c\n", *init_condition);
    }
#endif
}

/*---------------------------------------------------------------------
 * Function:  Generate_Init_Conditions
 * Purpose:   Genero le condizioni iniziali per ogni particella:
 massa, posizione x, posizione t, velocita x, velocita y
 * In args:
 *    num_particles:        numero totale di particelle
 * Out args:
 *    bodies:               array di strutture body
 */
struct body *Generate_Init_Conditions(const int num_particles)
{
    struct body *bodies;
    double mass = 5;
    double gap = 10;
    int i;

    if (!(bodies = malloc(num_particles * sizeof(struct body))))
    {
        printf("Impossibile allocare memoria per %d particelle.\n", num_particles);
        return NULL;
    }

    for (i = 0; i < num_particles; i++)
    {
        bodies[i].m = mass;
        bodies[i].vx = 0.0;
        bodies[i].vy = 0.0;
        bodies[i].fx = 0.0;
        bodies[i].fy = 0.0;

        bodies[i].x = i * gap;
        bodies[i].y = 0.0;

#ifdef READ_GENERATE_BODIES
        printf("body %d: x=%f y=%f\n", i, bodies[i].x, bodies[i].y);
#endif
    }

#ifdef GENERATE_INPUT_FILE
    FILE *fp_generate = fopen("generate_input.txt", "w+");
    if (!fp_generate)
    {
        printf("Inpossibile creare il file <%s>\n", "generate_input");
        exit(1);
    }

    for (i = 0; i < num_particles; i++)
    {
        fprintf(fp_generate, "%lf %lf %lf %lf %lf\n", bodies[i].m, bodies[i].x, bodies[i].y, bodies[i].vx, bodies[i].vy);
    }

    fclose(fp_generate);
#endif

    return bodies;
}

/*---------------------------------------------------------------------
 * Function:   Read_File_Init_Conditons
 * Purpose:    Legge da un file le infomazioni inziali per ogni particella:
 massa, posizione x, posizione y, velocita x, velocita y
 * In args:
 *    filename:             nome del file in input
 *    num_particles:        numero totale di particelle
 * Out args:
 *    bodies:               array di strutture body
 */
struct body *Read_File_Init_Conditons(char *filename, struct body *bodies, int num_particles)
{
    int i;

    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Impossibile aprire il file <%s>\n\n", filename);
        exit(1);
    }

    if (!((bodies) = malloc(num_particles * sizeof(struct body))))
    {
        printf("Impossibile allocare memoria per %d bodies", num_particles);
        exit(1);
    }

    for (i = 0; i < num_particles; i++)
    {
        fscanf(fp, "%le %le %le %le %le\n", &((bodies)[i].m), &((bodies)[i].x), &((bodies)[i].y), &((bodies)[i].vx), &((bodies)[i].vy));
    }
    fflush(fp);
    fclose(fp);

#ifdef DEBUG_READ_FILE
    for (i = 0; i < num_particles; i++)
    {
        //printf("%d %d\n", i, num_particles);
        printf("%lf %lf %lf %lf %lf\n", bodies[i].m, bodies[i].x, bodies[i].y, bodies[i].vx, bodies[i].vy);
    }
#endif

    return bodies;
}

/*---------------------------------------------------------------------
 * Function:   Output_State
 * Purpose:    Stampa lo stato corrente del sistema
 * In args:
 *    time:                 current time
 *    bodies:               array di tutte le strutture body
 *    num_particles:        numero totale di particelle
 */
void Output_State(double time, struct body *bodies, int num_particles)
{
    int i;

    if (my_rank == MASTER)
    {
        printf("Current time:%.2f\n", time);
        for (i = 0; i < num_particles; i++)
        {
            printf("Particle:%d\tX:%f", i, bodies[i].x);
            printf("\tY:%f", bodies[i].y);
            printf("\tVx:%f ", bodies[i].vx);
            printf("\tVy:%f\n", bodies[i].vy);
        }
        printf("\n");
    }
}

/*---------------------------------------------------------------------
 * Function:  Generate_Output_File
 * Purpose:   Genero un file di output con i risultati di ogni particella a termine della simulazione
 * In args:
 *    bodies:           array di tutte le strutture body
 *    num_particles:    numero totale di particelle
 */
void Generate_Output_File(struct body *bodies, int num_particles)
{
    int i;
    FILE *fp_generate_output = fopen("generate_output.txt", "w+");
    if (!fp_generate_output)
    {
        printf("Cannot create output file <%s>\n", "generate_output");
        exit(1);
    }

    for (i = 0; i < num_particles; i++)
        fprintf(fp_generate_output, "%lf %lf %lf %lf %lf\n", bodies[i].m, bodies[i].x, bodies[i].y, bodies[i].vx, bodies[i].vy);

    fflush(fp_generate_output);
    fclose(fp_generate_output);
}

double max(double a, double b)
{
    return (a > b ? a : b);
}

double min(double a, double b)
{
    return (a < b ? a : b);
}
